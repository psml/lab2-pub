#!/bin/bash

# check python3 is installed
python3 -V > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "python3 not installed"
fi

# check pip is installed
pip3 -V > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "pip3 not installed"
fi

# install virtualenv
pip3 install virtualenv

# create virtual environment
virtualenv -p /usr/bin/python3 .venv

# activate environment
source .venv/bin/activate

# ensure pip is installed
python3 -m ensurepip

# install required modules
python3 -m pip install -r requirements.txt

# run jupyter notebook
jupyter-notebook lab2.ipynb

